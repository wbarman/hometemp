package pro.sixguys.hometemp;

import org.junit.Test;


import java.util.Date;

import static java.lang.System.*;
import static org.junit.Assert.*;

public class SensorReplyTest {

    @Test
    public void classConstruction() throws Exception {
        /* reply example
        2, 1, 4, 13, -1, => header - skip this
        84, 50, 53, => T25
        102, 48,     => temp 24628 => 19.18
        120, 42,     => hum  33694 => 58.26
        3, -114,     => U = 2097.2 / AD_VAL => 2097.2 / 892 = 2.3511 V
        69, 110, 100  => End
        */

        byte[] btReply = {2, 1, 4, 13, -1, 84, 50, 53, 102, 48, 120, 42, 3, -114, 69, 110, 100, 0};

        SensorReply sensorReply = new SensorReply(btReply, new Date());
        out.printf("sensor name = %s \n", sensorReply.getName());
        out.printf("sensor timestamp = %s \n", sensorReply.getTimeStamp());
        out.printf("sensor temp = %f \n", sensorReply.getTemperature());
        out.printf("sensor hum = %f \n", sensorReply.getHumidity());
        out.printf("sensor V = %f \n", sensorReply.getBatteryVoltage());



    }

}