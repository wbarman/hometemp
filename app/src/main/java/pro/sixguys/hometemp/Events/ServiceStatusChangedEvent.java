package pro.sixguys.hometemp.Events;

import pro.sixguys.hometemp.Service.ServiceStatus;

public class ServiceStatusChangedEvent {

    private ServiceStatus serviceStatus;

    public ServiceStatus getServiceStatus() { return serviceStatus; }

    public ServiceStatusChangedEvent(ServiceStatus status) { serviceStatus = status; }
}
