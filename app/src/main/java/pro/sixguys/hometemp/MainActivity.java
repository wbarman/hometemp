package pro.sixguys.hometemp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import pro.sixguys.hometemp.Events.DataStorageUpdatedEvent;
import pro.sixguys.hometemp.Events.ServiceStatusChangedEvent;
import pro.sixguys.hometemp.Service.BTSensorService;
import pro.sixguys.hometemp.Service.ServiceStatus;

public class MainActivity extends AppCompatActivity {
    private View mainCoordinatorLayout;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    FloatingActionButton fabStartStopScan;
    Intent serviceIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mainCoordinatorLayout = findViewById(R.id.main_coordinator_layout);

        fabStartStopScan = (FloatingActionButton) findViewById(R.id.fabStartStopScan);
        fabStartStopScan.setOnClickListener(startStopScanButtonOnClickListener);

        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new SensorsSetAdapter(this, DataStorage.INSTANCE.getDataAsList());
        recyclerView.setAdapter(adapter);

        updateStartStopFabButtonIcon(BTSensorService.getServiceStatus());

        if (serviceIntent == null)
            serviceIntent = new Intent(this, BTSensorService.class);
    }

    private View.OnClickListener startStopScanButtonOnClickListener = new View.OnClickListener() {
        private final static int REQUEST_ENABLE_BT = 1;

        @Override
        public void onClick(View view) {
            if(BTSensorService.getServiceStatus() == ServiceStatus.UNINITIALIZED ||
                    BTSensorService.getServiceStatus() == ServiceStatus.STOPPED) {
                startService(serviceIntent);
            }
            else {
                stopService(serviceIntent);
            }
        }
    };

    // called after every data change in DataStorage
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataStorageUpdated(DataStorageUpdatedEvent event) {
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

        // refresh RecyclerView if there's data, after Activity restart or when device is woken up
        if (!DataStorage.INSTANCE.getDataAsList().isEmpty())
            adapter.notifyDataSetChanged();
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServiceStatusChanged(ServiceStatusChangedEvent event) {
        updateStartStopFabButtonIcon(event.getServiceStatus());
    }

    private void updateStartStopFabButtonIcon(ServiceStatus status) {
        switch (status) {
            case RUNNING:
                fabStartStopScan.setImageDrawable(getDrawable(R.drawable.ic_stop));
                break;
            case STOPPED:
            case UNINITIALIZED:
                fabStartStopScan.setImageDrawable(getDrawable(R.drawable.ic_start));
                break;
        }
    }
}
