package pro.sixguys.hometemp;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public enum BluetoothHelper {
    INSTANCE;

    private static final String TAG = "BluetoothHelper";

    private boolean fullyInitialized = false;

    private BluetoothAdapter bluetoothAdapter;
    private BluetoothManager bluetoothManager;
    private BluetoothLeScanner leScanner;
    ScanSettings.Builder scanSettingsBuilder;
    ScanSettings scanSettings;
    private boolean scanning;

    public boolean isBTTurnedOn() {
        return (bluetoothAdapter != null && bluetoothAdapter.isEnabled());
    }

    public int getBTState() { return bluetoothAdapter.getState(); }

    public boolean isFullyInitialized() { return fullyInitialized; }

    public boolean isScanningInProcess() {
        return scanning;
    }

    /**
     * Does initialization of BT on device and it even turns it on if it's off.
     *
     * @param context the context is used for getting Bluetooth system service
     * @return true if BT has been fully initialized
     */
    public boolean initBT(Context context) {
        try {
            if (!fullyInitialized) {
                bluetoothManager = (BluetoothManager) context.getSystemService(context.BLUETOOTH_SERVICE);
                bluetoothAdapter = bluetoothManager.getAdapter();

                switch (bluetoothAdapter.getState()) {
                    case BluetoothAdapter.STATE_OFF:
                        bluetoothAdapter.enable(); // async call
                        break;
                    case BluetoothAdapter.STATE_ON:
                        leScanner = bluetoothAdapter.getBluetoothLeScanner();

                        scanSettingsBuilder = new ScanSettings.Builder();
                        scanSettingsBuilder.setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY);
                        // scanSettingsBuilder.setMatchMode(ScanSettings.MATCH_NUM_MAX_ADVERTISEMENT);
                        scanSettings = scanSettingsBuilder.build();
                        fullyInitialized = true;
                        break;
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            throw e;
        }
        return fullyInitialized;
    }

    public void startScan() {
        if (!scanning) {
            scanning = true;
            List<ScanFilter> scanFilters = new ArrayList<>();
            leScanner.startScan(scanFilters, scanSettings, scanCallback);
        }
    }

    public void stopScan() {
        if (scanning) {
            scanning = false;
            if (leScanner != null && bluetoothAdapter.getState() == BluetoothAdapter.STATE_ON)
                leScanner.stopScan(scanCallback);
        }
    }

    public void resetHelper() {
        stopScan();
        fullyInitialized = false;
    }

    private ScanCallback scanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            if(bluetoothAdapter.getState() == BluetoothAdapter.STATE_ON) {
                ScanRecord btRecord = result.getScanRecord();

                if (btRecord != null) {
                    SensorReply sensorReply;

                    try {
                        sensorReply = new SensorReply(btRecord.getBytes(), new Date());
                    } catch (NullPointerException e) {
                        sensorReply = null;
                    }

                    if (sensorReply != null && sensorReply.isReplyValid())
                        DataStorage.INSTANCE.syncSensorData(sensorReply);
                }
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            switch (errorCode) {
                case ScanCallback.SCAN_FAILED_ALREADY_STARTED:
                    break;
                case ScanCallback.SCAN_FAILED_APPLICATION_REGISTRATION_FAILED:
                    break;
                case ScanCallback.SCAN_FAILED_FEATURE_UNSUPPORTED:
                    break;
                case ScanCallback.SCAN_FAILED_INTERNAL_ERROR:
                    break;
            }

        }
    };
}
