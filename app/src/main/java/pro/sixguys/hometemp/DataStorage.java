package pro.sixguys.hometemp;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.Observer;

import pro.sixguys.hometemp.Events.DataStorageUpdatedEvent;

public enum DataStorage {
    INSTANCE;

    // private static final String SENSORS = "sensors";
    // private SharedPreferences savedSensors;

    private List<SensorReply> sensorReplyList;
    private ArrayList<Observer> observers;

    DataStorage() {
        sensorReplyList = new ArrayList<>(10);
        observers = new ArrayList<>(10);
    }

    public List<SensorReply> getDataAsList() {
        return sensorReplyList;
    }

    public synchronized void syncSensorData(SensorReply reply) {
        int existingReplyIndex = sensorReplyList.indexOf(reply);

        if (existingReplyIndex == -1) {
            // add this reply to the list as there's no such sensor yet
            sensorReplyList.add(reply);
            notifySubscribers();
        }
        else {
            // update existing sensorReply
            if (reply.getTimeStamp().getTime() -
                    sensorReplyList.get(existingReplyIndex).getTimeStamp().getTime() > 1000) {
                sensorReplyList.set(existingReplyIndex, reply);
                notifySubscribers();
            }
        }
    }

    public void notifySubscribers() {
        EventBus.getDefault().post(new DataStorageUpdatedEvent());
    }

    // TODO: 5 - save and load data to file(s) for later analysis like graphs
    // savedSensors = getSharedPreferences(SENSORS, MODE_PRIVATE);
}
