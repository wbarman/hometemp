package pro.sixguys.hometemp;

import java.io.UnsupportedEncodingException;
import java.util.Date;

/* reply example in byte[]
        2, 1, 4, 13, -1, => header - skip this
        84, 50, 53, => T25
        102, 48,     => temp 24628 => 19.18
        120, 42,     => hum  33694 => 58.26
        3, -114,     => U = 2097.2 / AD_VAL => 2097.2 / 892 = 2.3511 V
        69, 110, 100  => End
*/
public class SensorReply {
    private String name; // e.g. "T23"
    private double temperature; // in Celsius
    private double humidity; // in %
    private double batteryVoltage; // in V
    private final Date timeStamp; // timestamp when the reply was received (i.e. time of object creation)
    private String replyEnding; // should be "End"

    public SensorReply(byte[] reply, Date timeStamp) {
        this.timeStamp = timeStamp;

        // parse BT reply
        // sensor name
        StringBuilder replyBeginning = new StringBuilder();
        for (int i = 5; i < 8; i++) {
            replyBeginning.append(signedByteToChar(reply[i]));
        }
        this.name = replyBeginning.toString();

        // reply end
        StringBuilder replyEnding = new StringBuilder();
        for (int i = 14; i < 17; i++) {
            replyEnding.append(signedByteToChar(reply[i]));
        }
        this.replyEnding = replyEnding.toString();

        int byteToIntResult;

        // sensor temperature t = -46.85 + 175.72*i/65536.00;
        byteToIntResult = (signedByteToUnsignedInt(reply[8]) * 256) + signedByteToUnsignedInt(reply[9]);
        this.temperature = -46.85 + ((175.72 * byteToIntResult) / 65536.00);

        // sensor humidity h = -6.00 + 125.00*i/65536.00
        byteToIntResult = (signedByteToUnsignedInt(reply[10]) * 256) + signedByteToUnsignedInt(reply[11]);
        this.humidity = -6 + ((125.00 * byteToIntResult) / 65536.00);

        // sensor voltage U = 2097.2 / AD_VAL
        byteToIntResult = (signedByteToUnsignedInt(reply[12]) * 256) + signedByteToUnsignedInt(reply[13]);
        this.batteryVoltage = 2097.2 / byteToIntResult;
    }

    public SensorReply(String name, double temperature, double humidity, double batteryVoltage, Date timeStamp) {
        this.name = name;
        this.temperature = temperature;
        this.humidity = humidity;
        this.batteryVoltage = batteryVoltage;
        this.timeStamp = timeStamp;
    }

    public String getName() {
        return name;
    }

    public double getTemperature() {
        return temperature;
    }

    public double getHumidity() {
        return humidity;
    }

    public double getBatteryVoltage() {
        return batteryVoltage;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public static int signedByteToUnsignedInt(byte inputByte) {
        return (int)(inputByte & 0xFF);
    }

    public static char signedByteToChar(byte inputByte) {
        return (char)signedByteToUnsignedInt(inputByte);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SensorReply that = (SensorReply) o;

        return getName().equals(that.getName());
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }

    public boolean isReplyValid() {
        boolean passed = false;

        // name validation && reply ending validation
        if(this.getName().matches("T[0-9][0-9]") &&
           this.replyEnding.matches("End")) {
            passed = true;
        }
        return passed;
    }
}
