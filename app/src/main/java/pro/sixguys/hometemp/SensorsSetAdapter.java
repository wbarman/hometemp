package pro.sixguys.hometemp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class SensorsSetAdapter extends RecyclerView.Adapter<SensorsSetAdapter.ViewHolder> {
    private List<SensorReply> sensorReplyList;
    private Context context;
    private RecyclerView.ViewHolder holder;
    private int position;

    public SensorsSetAdapter(Context context, List<SensorReply> sensorReplyList) {
        this.context = context;
        this.sensorReplyList = sensorReplyList;
    }

    public Context getContext() {
        return this.context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View sensorView = inflater.inflate(R.layout.list_item, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(sensorView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SensorReply sensorReply = sensorReplyList.get(position);

        holder.textViewSensorName.setText(sensorReply.getName());
        holder.textViewTemperature.setText(String.format("%.1f", sensorReply.getTemperature()));
        holder.textViewHumidity.setText(String.format("%.1f", sensorReply.getHumidity()));
        holder.textViewTimeStamp.setText(String.format("%tc", sensorReply.getTimeStamp()));
        holder.textViewVoltage.setText(String.format("%.2f", sensorReply.getBatteryVoltage()));
    }

    @Override
    public int getItemCount() {
        return this.sensorReplyList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewSensorName;
        private TextView textViewTemperature;
        private TextView textViewHumidity;
        private TextView textViewTimeStamp;
        private TextView textViewVoltage;

        public ViewHolder(View itemView) {
            super(itemView);

            textViewSensorName = (TextView) itemView.findViewById(R.id.textViewSensorName);
            textViewTemperature = (TextView) itemView.findViewById(R.id.textViewTemperature);
            textViewHumidity = (TextView) itemView.findViewById(R.id.textViewHumidity);
            textViewTimeStamp = (TextView) itemView.findViewById(R.id.textViewTimeStamp);
            textViewVoltage = (TextView) itemView.findViewById(R.id.textViewVoltage);
        }
    }
}
