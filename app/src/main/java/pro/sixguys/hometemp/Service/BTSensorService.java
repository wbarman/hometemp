package pro.sixguys.hometemp.Service;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;

import org.greenrobot.eventbus.EventBus;

import pro.sixguys.hometemp.BluetoothHelper;
import pro.sixguys.hometemp.Events.ServiceStatusChangedEvent;
import pro.sixguys.hometemp.MainActivity;
import pro.sixguys.hometemp.R;

public class BTSensorService extends Service {

    private static final String TAG = "BTSensorService";
    private final int FOREGROUND_NOTIFICATION_ID = 123;
    private static ServiceStatus serviceStatus;

    static {
        serviceStatus = ServiceStatus.UNINITIALIZED;
    }

    public static ServiceStatus getServiceStatus() { return serviceStatus; }

    private void setServiceStatus(ServiceStatus status) {
        serviceStatus = status;
        EventBus.getDefault().post(new ServiceStatusChangedEvent(status));
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setServiceStatus(ServiceStatus.STOPPED);

        // register for broadcasting of BT changes
        IntentFilter btFilter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(btBroadcastReceiver, btFilter);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        setServiceStatus(ServiceStatus.RUNNING);
        startForeground(FOREGROUND_NOTIFICATION_ID, buildNotification("BTSensorService just started." +
                " Status: " + getServiceStatus().toString(), true));

        if (BluetoothHelper.INSTANCE.initBT(this)) {
            // start async work
            updateNotificationText("BTSensorService: Scanning for sensors started.", true);
            BluetoothHelper.INSTANCE.startScan();
        }
        else {
            updateNotificationText("BTSensorService: Waiting for BT status change.", true);
        }

        // start service if it's killed after start
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        updateNotificationText("BTSensorService just stopped.", false);
        unregisterReceiver(btBroadcastReceiver);
        setServiceStatus(ServiceStatus.STOPPED);
        BluetoothHelper.INSTANCE.resetHelper();
        stopForeground(true);
        stopSelf();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private Notification buildNotification(String notificationText, boolean isRunning) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 111, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentTitle(getString(R.string.app_service))
                .setContentText(notificationText)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setContentIntent(pendingIntent);
        if (isRunning) {
            builder.setOngoing(true)
                    .setSmallIcon(android.R.drawable.ic_media_play);
        }
        else {
            builder.setOngoing(false)
                    .setSmallIcon(android.R.drawable.ic_dialog_info);
        }
        return builder.build();
    }

    private void updateNotificationText(String newNotificationText, boolean isRunning) {
        Notification newNotification = buildNotification(newNotificationText, isRunning);
        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(FOREGROUND_NOTIFICATION_ID, newNotification);
    }

    // receives bluetooth changes broadcasted by BluetoothAdapter
    private final BroadcastReceiver btBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                final String action = intent.getAction();

                if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                    final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                    switch (state) {
                        case BluetoothAdapter.STATE_OFF:
                            if (getServiceStatus() == ServiceStatus.RUNNING &&
                                    !BluetoothHelper.INSTANCE.isFullyInitialized()) {
                                updateNotificationText("BT is off, trying to turn it on", true);
                                BluetoothHelper.INSTANCE.initBT(BTSensorService.this);
                            }
                            break;
                        case BluetoothAdapter.STATE_ON:
                            if (getServiceStatus() == ServiceStatus.RUNNING) {
                                BluetoothHelper.INSTANCE.initBT(BTSensorService.this);

                                if (BluetoothHelper.INSTANCE.isFullyInitialized() &&
                                        !BluetoothHelper.INSTANCE.isScanningInProcess()) {
                                    updateNotificationText("BT is fully initialized, starting scanning",
                                            true);
                                    BluetoothHelper.INSTANCE.startScan();
                                }
                            }
                            break;
                        case BluetoothAdapter.STATE_TURNING_OFF:
                            if (getServiceStatus() == ServiceStatus.RUNNING) {
                                BluetoothHelper.INSTANCE.resetHelper();
                                updateNotificationText("BTSensorService: BT is turning off.", true);
                            }
                            break;
                        case BluetoothAdapter.STATE_TURNING_ON:
                            if (getServiceStatus() == ServiceStatus.RUNNING)
                                updateNotificationText("BTSensorService: BT is turning on.", true);
                            break;
                    }

                }
            }
        }
    };
}
