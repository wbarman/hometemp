package pro.sixguys.hometemp.Service;

public enum ServiceStatus {
    UNINITIALIZED, STOPPED, RUNNING
}
